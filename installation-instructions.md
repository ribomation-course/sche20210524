# Installation Instructions

In order to participate and perform the programming exercises of the course, 
you need to have the following installed.

## Zoom Client
* https://us02web.zoom.us/download
* [Video/Audio Configuration](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/zoom-configuration.md)

## GIT Client
* https://git-scm.com/downloads

## IDE
A decent IDE, such as any of

* JetBrains IntellJ IDEA (*Community*)
    - https://www.jetbrains.com/idea/download
* MicroSoft Visual Code
    - https://code.visualstudio.com/download


## Java JDK
The latest version of Java JDK installed. 
* [Java JDK Download](https://adoptopenjdk.net/)

Go for the latest version and the HotSpot engine.
* OpenJDK 16
* HotSpot


