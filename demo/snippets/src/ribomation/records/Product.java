package ribomation.records;
import java.util.Properties;

public record Product(String name, float price) {
    private static int VAT_percent;

    static {
        var is = Product.class.getResourceAsStream("business-data.properties");
        try (is) {
            var data = new Properties();
            data.load(is);
            VAT_percent = Integer.parseInt(data.getProperty("vat.percent"));
        } catch (Exception e) {
            VAT_percent = -1;
        }
    }

    public static void setVAT(int vat) {
        if (0 < vat && vat < 100) VAT_percent = vat;
    }
    public static int VAT() { return VAT_percent; }

    public float price() {
        return (float) (price * (1 + VAT_percent / 100.0));
    }
}

