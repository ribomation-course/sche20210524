package ribomation.records;

import java.util.Objects;

public class Coordinate implements Comparable<Coordinate> {
    private final int x, y;

    public Coordinate(int x, int y) {
        if (x < 0 || y < 0) {
            throw new IllegalArgumentException(String.format("Must be non-negative [%d, %d]", x, y));
        }
        this.x = x;
        this.y = y;
    }

    public int getX() { return x; }
    public int getY() { return y; }

    @Override
    public String toString() {
        return String.format("Coord[x=%d, y=%d]", x, y);
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof Coordinate that) &&  this.x == that.x && this.y == that.y;
    }

    @Override
    public int hashCode() { return Objects.hash(x, y); }

    @Override
    public int compareTo(Coordinate that) {
        if (this.x != that.x) return Integer.compare(this.x, that.x);
        return Integer.compare(this.y, that.y);
    }
}


