package ribomation.switch_expr;

import java.time.LocalDate;
import java.time.Month;

public class UsingSwitchExpr {
    public static void main(String[] args) {
        var app = new UsingSwitchExpr();
        app.run();
    }

    void run() {
        var result = quarterOf(LocalDate.now().getMonth());
        System.out.printf("(a) Currently it's the %s%n", result);

        var result2 = quarterOfStr(LocalDate.now().plusMonths(2).getMonth().toString());
        System.out.printf("(b) Next it's the %s%n", result2);
    }

    String quarterOf(Month month) {
        var nth = switch (month) {
            case JANUARY, FEBRUARY, MARCH -> "1st";
            case APRIL, MAY, JUNE -> "2nd";
            case JULY, AUGUST, SEPTEMBER -> "3rd";
            case OCTOBER, NOVEMBER, DECEMBER -> "4th";
            default -> throw new IllegalArgumentException("invalid month");
        };
        return nth + " Quarter";
    }

    String quarterOfStr(String month) {
        var nth = switch (month) {
            case "JANUARY", "FEBRUARY", "MARCH" -> "1st";
            case "APRIL", "MAY", "JUNE" -> "2nd";
            case "JULY", "AUGUST", "SEPTEMBER" -> "3rd";
            case "OCTOBER", "NOVEMBER", "DECEMBER" -> "4th";
            default -> throw new IllegalArgumentException("invalid month");
        };
        return nth + " Quarter";
    }


}


