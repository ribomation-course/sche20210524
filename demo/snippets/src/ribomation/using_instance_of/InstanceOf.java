package ribomation.using_instance_of;

import java.util.*;

public class InstanceOf {
    public static void main(String[] args) {
        var app = new InstanceOf();
        app.run1();
        System.out.println("-".repeat(40));
        app.run2();
    }

    void run1() {
        var p1 = new Person("Nisse", 42);
        var p2 = new Person("Nisse", 42);
        System.out.printf("(a) p1 == p2: %b%n", p1.equals(p2));

        p2.age++;
        System.out.printf("(b) p1 == p2: %b%n", p1.equals(p2));
    }

     void run2() {
         var n = 100;
         var personList = mkPersons(n);
         System.out.printf("List<>.size = %d %n", n);

         var personSet = new HashSet<>(personList);
         System.out.printf("Set<>.size = %d%n", personSet.size());

         personList.removeIf(person -> personList.stream().filter(p -> p.equals(person)).count() == 1);
         System.out.println("Duplicates:");
         new TreeSet<>(personList).forEach(System.out::println);
     }

    List<Person> mkPersons(int n) {
        var lst = new ArrayList<Person>();
        while (n-- > 0) lst.add(mkPerson());
        return lst;
    }

    Person mkPerson() {
        var name = names[r.nextInt(names.length)];
        var age = r.nextInt(80) + 20;
        return new Person(name, age);
    }

    static final String[] names = {
            "Anna", "Berit", "Carin", "Doris", "Eva",
            "Adam", "Bertil", "Carl", "Dennis", "Erik",
    };
    static final Random r = new Random(System.nanoTime() % 997);

}
