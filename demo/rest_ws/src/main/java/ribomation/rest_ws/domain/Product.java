package ribomation.rest_ws.domain;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

public class Product {
    private long   id;
    private String name;
    private float  price;
    private int    items;
    private Date   modified;

    public Product() {
    }

    public Product(long id, String name, float price, int items, Date modified) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.items = items;
        this.modified = modified;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", items=" + items +
                ", modified=" + modified +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return getId() == product.getId() &&
                Float.compare(product.getPrice(), getPrice()) == 0 &&
                getItems() == product.getItems() &&
                Objects.equals(getName(), product.getName()) &&
                Objects.equals(getModified(), product.getModified());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getPrice(), getItems(), getModified());
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public float getPrice() {
        return price;
    }

    public int getItems() {
        return items;
    }

    public Date getModified() {
        return modified;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setItems(int items) {
        this.items = items;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }
}
