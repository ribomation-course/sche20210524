package ribomation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

import static java.lang.Integer.compare;
import static java.lang.Integer.parseInt;
import static java.util.Comparator.comparing;
import static java.util.Comparator.comparingInt;

public class App {
    public static void main(String[] args) throws IOException {
        var app = new App();
        var persons = app.load("./data/persons.csv", ";");
//        app.run1(persons);
//        app.run2(persons);
        app.run3(persons);
        persons.forEach(System.out::println);
    }

    private List<Person> load(String csvFile, String sep) throws IOException {
        var result = new ArrayList<Person>();
        Files.readAllLines(Path.of(csvFile)).forEach(line -> {
            // firstName;lastName;age
            // Faîtes;O'Shiels;84
            if (!line.startsWith("firstName")) {
                var f = line.split(sep);
                result.add(new Person(f[0], f[1], parseInt(f[2])));
            }
        });
        return result;
    }

    void run1(List<Person> persons) {
        Comparator<Person> byName = (left, right) -> left.lastName().compareTo(right.lastName());
        Comparator<Person> byAge = (left, right) -> Integer.compare(left.age(), right.age());
        Comparator<Person> byNameAgeReversed = byName.thenComparing(byAge).reversed();
        persons.sort(byNameAgeReversed);
    }

    void run2(List<Person> persons) {
        Comparator<Person> byName = comparing(Person::lastName);
        Comparator<Person> byAge = comparingInt(Person::age);
        persons.sort(byName.thenComparing(byAge).reversed());
    }

    void run3(List<Person> persons) {
        persons.sort(comparing(Person::lastName).thenComparingInt(Person::age).reversed());
    }

}
