package ribomation.part_2;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.run();
    }

    void run() {
        var strings = List.of("apple", "", "coco nut", "", "", "pear");

        use("Anon Class", strings, new Predicate<String>() {
            @Override
            public boolean test(String s) {
                return s.isEmpty();
            }
        });

        use("Lambda", strings, s -> s.isEmpty());

        use("Method Ref", strings, String::isEmpty);
    }

    void use(String what, List<String> strings, Predicate<String> p) {
        System.out.printf("%-12s: %s --> %s%n", what, strings, filter(strings, p));
    }

    List<String> filter(List<String> strings, Predicate<String> p) {
        var result = new ArrayList<String>();
        strings.forEach(s -> {
            if (!p.test(s)) result.add(s);
        });
        return result;
    }

}
