package ribomation.part_1;

public interface StringTransform {
    String transform(String s);
}
