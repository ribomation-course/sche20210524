package ribomation.part_1;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.run("Tjabba Habba");
    }

    void run(String sample) {
        use("Named Class", sample, new UC());

        use("Anon Class", sample, new StringTransform() {
            @Override
            public String transform(String s) {
                return s.toUpperCase();
            }
        });

        use("Lambda", sample, s -> s.toUpperCase());

        use("Method Ref", sample, String::toUpperCase);
    }

    void use(String what, String s, StringTransform t) {
        System.out.printf("%-12s: %s --> %s%n", what, s, t.transform(s));
    }

    static class UC implements StringTransform {
        @Override
        public String transform(String s) {
            return s.toUpperCase();
        }
    }

}
