package ribomation.part_3;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.function.UnaryOperator;

public class TransformApp {
    public static void main(String[] args) {
        var app = new TransformApp();
        app.run();
    }

    void run() {
        use("Ints", List.of(1, 2, 3, 4, 5), x -> x * x);
        use("Strings", List.of("apple", "banana", "coco nut"), String::toUpperCase);
        use("Dates", List.of(
                LocalDate.of(2021, Month.JANUARY, 10),
                LocalDate.of(2021, Month.FEBRUARY, 10),
                LocalDate.of(2021, Month.MARCH, 10),
                LocalDate.of(2021, Month.APRIL, 10)),
                d -> d.plusDays(20)
        );
    }

    <T> void use(String what, List<T> lst, UnaryOperator<T> f) {
        System.out.printf("%-8s: %s --> %s%n", what, lst, transform(lst, f));
    }

    <T> List<T> transform(List<T> lst, UnaryOperator<T> f) {
        var result = new ArrayList<T>();
        lst.forEach(value -> result.add(f.apply(value)));
        return result;
    }
}
