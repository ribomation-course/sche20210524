package ribomation.part_3;

import java.util.function.Consumer;

public class RepeatApp {
    public static void main(String[] args) {
        var app = new RepeatApp();
        app.run();
    }

    void run() {
        repeat(5, n -> System.out.printf("(%d) Lambdas are cool%n", n));
        repeat2(5, n -> System.out.printf("(%d) Lambdas are cool%n", n));
    }

    void repeat(int count, Consumer<Integer> expr) {
        for (var k = 1; k <= count; ++k) expr.accept(k);
    }

    void repeat2(int count, Tjabba expr) {
        for (var k = 1; k <= count; ++k) expr.getIt(k);
    }

    interface Tjabba {
        void getIt(int n);
    }

}
