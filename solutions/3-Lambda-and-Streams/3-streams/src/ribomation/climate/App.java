package ribomation.climate;

import ribomation.util.Elapsed;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.zip.GZIPInputStream;

import static java.util.stream.Collectors.averagingDouble;
import static java.util.stream.Collectors.groupingBy;

public class App {
    static CountingInputStream compressed;
    static CountingInputStream uncompressed;

    public static void main(String[] args) {
        var result = new Elapsed("Data ").time(() -> load(Path.of("./data/climate-data.txt.gz")));
        System.out.printf("Bytes: %.0f MB (%.0f MB compressed)%n",
                uncompressed.byteCount() * 1E-6, compressed.byteCount() * 1E-6
        );
        result.forEach((year, avgTemp) -> System.out.printf(Locale.ENGLISH, "%d: %.1f%n", year, avgTemp));
    }

    static Map<Integer, Double> load(Path file) {
        var reader = open(file);
        try (reader) {
            return reader.lines()
                    .parallel()
                    .filter(line -> !line.startsWith("STN---"))
                    .map(line -> line.split("\\s+"))
                    .map(arr -> new Data(arr[2], arr[3]))
                    .collect(groupingBy(Data::year, TreeMap::new, averagingDouble(Data::celsius)))
                    ;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    static BufferedReader open(Path file) {
        try {
            return new BufferedReader(
                    new InputStreamReader(
                            uncompressed = new CountingInputStream(
                                    new GZIPInputStream(
                                            compressed = new CountingInputStream(
                                                    Files.newInputStream(file))))));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
