package ribomation.climate;

public record Data(String date, String temp) {
    int year() {
        return Integer.parseInt(date().substring(0, 4));
    }

    double celsius() {
        var fahrenheit = Double.parseDouble(temp());
        return (5D / 9D) * (fahrenheit - 32D);
    }
}
