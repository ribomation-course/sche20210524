package ribomation.util;

import java.util.function.Supplier;

public class Elapsed {
    private final String what;
    private long elapsedNanoSecs = 0;

    public Elapsed(String what) {
        this.what = what;
    }

    public <T> T compute(Supplier<T> stmts) {
        var startTime = System.nanoTime();
        try {
            return stmts.get();
        } finally {
            var endTime = System.nanoTime();
            elapsedNanoSecs = endTime - startTime;
        }
    }

    public <T> T time(Supplier<T> stmt) {
        try {
            return compute(stmt);
        } finally {
            System.out.println(this);
        }
    }

    public float elapsedSecs() {
        return (float) (elapsedNanoSecs * 1E-9);
    }

    public String what() {
        return what;
    }

    @Override
    public String toString() {
        return String.format("%s: elapsed %.3f seconds", what(), elapsedSecs());
    }
}
