package ribomation.persons;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.Integer.parseInt;
import static java.lang.Short.parseShort;
import static java.util.stream.Collectors.toList;

public class PersonsApp2 {
    public static void main(String[] args) {
        var app = new PersonsApp2();
        var list = app.load(Path.of("./data/persons.csv"), ";");
        list.forEach(System.out::println);
    }

    List<Person> load(Path csvFile, String sep) {
        return lines(csvFile)
                .skip(1)
               // .map(line -> Person.fromCSV(line, sep))
                .map(line -> line.split(";"))
                .map(f -> new Person(f[0], f[1].equals("Female"), parseShort(f[2]), parseInt(f[3])))
                .filter(Person::female)
                .filter(p -> 30 <= p.age() && p.age() <= 40)
                .filter(p -> p.postCode() < 20_000)
                //.peek(System.out::println)
                //.count()
                .collect(toList())
                ;
    }

    private Stream<String> lines(Path file) {
        try {
            return Files.lines(file);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


}
