package ribomation.persons;

import ribomation.util.Elapsed;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class PersonsApp {
    public static void main(String[] args) throws IOException {
        var app = new PersonsApp();
        var people = new Elapsed("Load CSV").time(() -> app.load(Path.of("./data/persons.csv")));
        app.print(people);
    }

    private List<Person> load(Path csvFile)  {
        return lines(csvFile)
                .skip(1)
                .map(Person::fromCSV)
                .filter(Person::female)
                .filter(p -> 30 <= p.age() && p.age() <= 40)
                .filter(p -> p.postCode() < 20_000)
                .collect(toList());
    }

    private void print(List<Person> people) {
        people.forEach(System.out::println);
    }

    private Stream<String> lines(Path file) {
        try {
            return Files.lines(file);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
