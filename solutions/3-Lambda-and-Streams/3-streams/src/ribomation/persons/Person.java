package ribomation.persons;

import static java.lang.String.format;

public record Person(String name, boolean female, short age, int postCode) {
    @Override
    public String toString() {
        var zip = Integer.toString(postCode());
        return format("Person{%s, %s (%s) @ %s}",
                name(),
                age(),
                (female() ? "fe" : "") + "male",
                format("%3s %2s", zip.substring(0,3), zip.substring(3,5))
        );
    }

    public static Person fromCSV(String csv) {
        return fromCSV(csv, ";");
    }

    public static Person fromCSV(String csv, String sep) {
//        name;gender;age;postCode
//        Nevil Simenel;Female;74;61748
        var f = csv.split(sep);
        var name = f[0];
        var female = f[1].equalsIgnoreCase("female");
        var age = Short.parseShort(f[2]);
        var pc = Integer.parseInt(f[3]);
        return new Person(name, female, age, pc);
    }
}
