package ribomation.words;

import ribomation.util.Elapsed;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static java.lang.String.format;
import static java.lang.System.lineSeparator;
import static java.util.Comparator.comparingLong;
import static java.util.stream.Collectors.*;

public class WordCloud {
    record WordFreq(String word, long count) { }

    public static void main(String[] args) throws Exception {
        var app = new WordCloud();

        var file = args.length == 0 ? "./data/musketeers.txt" : args[0];
        var name = Path.of(file).getFileName().toString();
        name = name.substring(0, name.lastIndexOf('.') );
        var outfile = "./out/" + name + ".html";

        var maxWord = 200;
        var minLength = 5;
        var minFont = 10;
        var maxFont = 100;
        var fontUnit = "pt";

        var result = new Elapsed("Load & Aggregate").time(() -> {
            var words = app.load(Path.of(file), maxWord, minLength);
            return app.toTags(words, minFont, maxFont, fontUnit);
        });
        app.store(result, Path.of(outfile), name);
    }

    List<WordFreq> load(Path file, int maxWords, int minLength) {
        final var whiteSpace = Pattern.compile("[^a-zA-Z]+");
        return lines(file)
                .flatMap(whiteSpace::splitAsStream)
                .filter(w -> w.length() >= minLength)
                .collect(groupingBy(String::toLowerCase, counting()))
                .entrySet().stream()
                .map(pair -> new WordFreq(pair.getKey(), pair.getValue()))
                .sorted(comparingLong(WordFreq::count).reversed())
                .limit(maxWords)
                .collect(toList())
                ;
    }

    List<String> toTags(List<WordFreq> words, int minFont, int maxFont, String fontUnit) {
        var maxFreq = words.get(0).count();
        var minFreq = words.get(words.size() - 1).count();
        var scale = (double) (maxFont - minFont) / (maxFreq - minFreq);
        var r = new Random(System.nanoTime() % 12345);
        Supplier<String> color = () -> String.format("#%02X%02X%02X",
                r.nextInt(256), r.nextInt(256), r.nextInt(256));

        return words.stream()
                .map(wf -> format("<span style=\"font-size: %d%s; color: %s;\">%s</span>",
                        (int) (scale * wf.count()) + minFont,
                        fontUnit,
                        color.get(),
                        wf.word()))
                .sorted((_a, _b) -> r.nextBoolean() ? -1 : +1)
                .collect(toList())
                ;
    }

    private void store(List<String> tags, Path outfile, String name) throws IOException {
        var prefix = """
                <html>
                <head>
                    <title>Word Cloud</title>
                </head>
                <body>
                    <h1>Word Cloud: @NAME@</h1>
                """.stripIndent().replace("@NAME@", name);
        var suffix = """
                </body>
                </html>
                """.stripIndent();
        var html = tags.stream().collect(joining(lineSeparator(), prefix, suffix));
        Files.writeString(outfile, html);
        System.out.printf("written %s%n", outfile);
    }

    Stream<String> lines(Path file) {
        try {
            return Files.lines(file);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
