package ribomation;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.System.lineSeparator;
import static java.util.concurrent.CompletableFuture.supplyAsync;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

public class StackOverflow {
    public static void main(String[] args) {
        var app = new StackOverflow();
        app.run();
    }

    String tagsFile = "./data/tags.txt";
    String htmlFile = "./data/result.html";
    String outDir = "./out";
    int maxQuestions = 5;
    String soUrl = "https://stackoverflow.com/questions/tagged/";

    void run() {
        var startTime = System.nanoTime();

        supplyAsync(() -> tags().map(tag -> fetchTopics(tag)))
                .thenApply(futures -> futures.map(f -> f.join()))
                .thenApply(results -> results.map(r -> r.asHTML()))
                .thenApply(snippets -> snippets.collect(joining(lineSeparator())))
                .thenApply(body -> toHTML(body))
                .thenApply(html -> {
                    var elapsedNanoSecs = System.nanoTime() - startTime;
                    System.out.printf("elapsed: %.2f seconds%n", elapsedNanoSecs * 1E-9);
                    return html;
                })
                .thenAccept(html -> saveHtmlFile(html))
                .exceptionally(err -> {
                    err.printStackTrace();
                    return null;
                })
                .join()
        ;
    }

    CompletableFuture<Result> fetchTopics(String tag) {
        Supplier<Result> jsoupTask = () -> {
            try {
                var topics = Jsoup.connect(soUrl + tag).get()
                        .select("a.question-hyperlink").stream()
                        .limit(maxQuestions)
                        .map(Element::text)
                        .collect(toList());
                return new Result(tag, topics);
            } catch (IOException x) {
                throw new RuntimeException(x);
            }
        };

        return supplyAsync(jsoupTask);
    }

    Stream<String> tags() {
        try {
            return Files.lines(Path.of(tagsFile));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    String loadHtmlTemplate() {
        try {
            return Files.readString(Path.of(htmlFile));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    void saveHtmlFile(String html) {
        try {
            var file = Path.of(outDir, "stack-overflow.html");
            Files.writeString(file, html);
            System.out.printf("written: %s%n", file);
        } catch (IOException x) {
            throw new RuntimeException(x);
        }
    }

    String toHTML(String body) {
        var html = """
                <!DOCTYPE html>
                <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
                          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
                    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
                            integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
                            crossorigin="anonymous"></script>
                    <title>@TITLE@</title>
                </head>
                <body>
                    <div class="container">
                        <nav class="navbar navbar-inverse bg-primary">
                            <a class="navbar-brand" href="#">ASYNC Computation: @NAME@</a>
                        </nav>
                        <h1 class="text-warning my-3">@TITLE@</h1>
                        <p class="text-muted mb-2">Generated @DATE@</p>
                """.stripIndent();
        var rest = """
                    </div>
                </body>
                </html>
                """.stripIndent();

        html = substitute(html, "TITLE", "Stack OverFlow Tags");
        html = substitute(html, "NAME", "Using CompletableFuture");
        html = substitute(html, "DATE", String.format("%1$tF %1$tR", new Date()));

        return html + body + rest;
    }

    String substitute(String html, String key, String value) {
        try {
            return html.replaceAll("@" + key + "@", value);
        } catch (Exception x) {
            System.out.printf("ERR: %s%n", x);
            System.out.printf("key: %s, value: %s", key, value);
            return html;
        }
    }

}
