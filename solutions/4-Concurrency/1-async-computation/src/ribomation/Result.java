package ribomation;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.joining;

public record Result(String tag, List<String> topics) {
    public static Result create(String tag) {
        return new Result(tag, new ArrayList<>());
    }

    public String asHTML() {
        var html = """
                <h3 class="bg-success text-white p-1">Tag: %s</h3>
                <ul> %s </ul>
                """.stripIndent();
        return String.format(html, tag,
                topics().stream().map(topic -> "<li>" + topic + "</li>").collect(joining())
        );
    }
}
