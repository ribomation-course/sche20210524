import java.util.Arrays;
import java.util.List;

public class ShoppingCart {
    public static void main(String[] args) {
        System.out.printf("(a) %s%n", summary(Arrays.asList()));
        System.out.printf("(b) %s%n", summary(Arrays.asList("apple")));
        System.out.printf("(c) %s%n", summary(Arrays.asList("apple", "banana", "coco nut")));
        System.out.printf("(d) %s%n", summary(Arrays.asList("apple", "banana", "coco nut", "date plum", "orange", "pear")));
    }

    static String summary(List<Object> cart) {
        return switch (cart.size()) {
            case 0 -> "No items in cart";
            case 1 -> "One item in cart";
            case 2, 3, 4 -> "Few items in cart";
            default -> "Many items in cart";
        };
    }
}
