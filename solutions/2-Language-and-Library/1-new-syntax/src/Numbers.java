import java.util.ArrayList;
import static java.lang.Integer.parseInt;

public class Numbers {
    public static void main(String[] args) {
        var n = args.length == 0 ? 10 : parseInt(args[0]);

        var ints = new ArrayList<Integer>();
        for (var k = 0; k < n; ++k) ints.add(k + 1);
        System.out.printf("ints: %s%n", ints);

        var reals = new ArrayList<Double>();
        for (var k = 0; k < n; ++k) reals.add(ints.get(k) * Math.PI);
        System.out.print("reals: ");
        for (var k = 0; k < n; ++k) System.out.printf("%.3f ", reals.get(k));
        System.out.println();

        var strings = new ArrayList<String>();
        for (var k = 0; k < n; ++k) strings.add(reals.get(k).toString());
        System.out.print("strings: ");
        for (var k = 0; k < n; ++k) System.out.printf("'%s' ", strings.get(k));
        System.out.println();
    }
}
