public class ProductApp {
    public static void main(String[] args) {
        var p1 = new Product("apple", 1.23F, 5);
        var p2 = new Product("apple", 1.23F, 5);
        System.out.printf("%s == %s --> %b%n", p1, p2, p1.equals(p2));

        p2.count++;
        System.out.printf("%s == %s --> %b%n", p1, p2, p1.equals(p2));
    }
}
