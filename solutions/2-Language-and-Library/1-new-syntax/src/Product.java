import java.util.Objects;

public class Product {
    String name;
    float price;
    int count;

    public Product(String name, float price, int count) {
        this.name = name;
        this.price = price;
        this.count = count;
    }

    @Override
    public String toString() {
        return String.format("Product{%s, %.2f kr, %d in stock}", name, price, count);
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof Product p)
                && name.equals(p.name)
                && Float.compare(price, p.price) == 0
                && count == p.count;

//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        Product product = (Product) o;
//        return Float.compare(product.price, price) == 0 && count == product.count && Objects.equals(name, product.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, price, count);
    }
}
