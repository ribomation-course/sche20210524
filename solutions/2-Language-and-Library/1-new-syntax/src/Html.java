public class Html {
    public static void main(String[] args) {
        var html = """
                <html>
                <head>
                    <title>Silly HTML</title>
                </head>
                <body>
                    <h1> Silly HTML </h1>
                    <p>
                        Just a bunch of <em>words</em>.
                    </p>
                </body>
                </html>
                """.stripIndent();
        System.out.println(html);
    }
}
