import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class MoviesApp {
    public static void main(String[] args) throws Exception {
        var app = new MoviesApp();
        app.load(Path.of("./data/movies.csv"), ";");
        app.print(5);
    }

    record Movie(int id, String title, String genres, float price) { }

    List<Movie> movieList = new ArrayList<>();

    void load(Path csvFile, String SEP) throws IOException {
        var lines = Files.readAllLines(csvFile);
        movieList = new ArrayList<>(lines.size());

        lines.subList(1, lines.size()).forEach(line -> {
//            id;title;genres;price
//            1;Stage Fright (Deliria);Horror;496.05
            var f = line.split(SEP);
            var ix = 0;
            var id = Integer.parseInt(f[ix++]);
            var title = f[ix++];
            var genres =  f[ix++];
            var price = Float.parseFloat(f[ix++]);
            movieList.add(new Movie(id, title, genres, price));
        });
    }

    void print(int max) {
        movieList.sort(new MovieComparator());
        movieList.subList(0, max).forEach(System.out::println);
    }

    private static class MovieComparator implements Comparator<Movie> {
        @Override
        public int compare(Movie lhs, Movie rhs) {
            return Float.compare(rhs.price, lhs.price);
        }
    }
}
