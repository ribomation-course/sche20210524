package ribomation;

public record Item(Product product, int count) {
    public Item {
        if (count < 0) {
            throw new IllegalArgumentException("negative count: " + count);
        }
    }

    public float total() {
        return count * product().price();
    }

    @Override
    public String toString() {
        return String.format("%s x %d --> %.2f kr", product, count, total());
    }
}
