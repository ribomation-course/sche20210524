package ribomation;

public record Product(String name, float price) {
    static int VAT = 25;

    @Override
    public float price() {
        return (float) (price * (1 + VAT / 100.0));
    }

    @Override
    public String toString() {
        return String.format("%s: %.2f [%.2f] kr", name, price(), price);
    }
}
