package ribomation;
import java.util.ArrayList;
import java.util.List;
import static java.lang.String.format;
import static java.lang.System.lineSeparator;
import static java.util.stream.Collectors.joining;

public class ShoppingCart {
    private final List<Item> items;

    public ShoppingCart() {
        items = new ArrayList<>();
    }

    void add(Item item) {
        items.add(item);
    }

    void clear() {
        items.clear();
    }

    float total() {
        return (float) items.stream().mapToDouble(Item::total).sum();
    }

    @Override
    public String toString() {
        return format("-- shopping cart --%n%s",
                items.stream().map(Item::toString).collect(joining(lineSeparator())));
    }
}
