package ribomation;
import java.util.Random;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.run();
    }

    void run() {
        var r = new Random();
        var apple = new Product("Apple", r.nextFloat() * 100);
        var banana = new Product("Banana", r.nextFloat() * 100);
        var cocoNut = new Product("Coco Nut", r.nextFloat() * 100);
        var orange = new Product("Orange", r.nextFloat() * 100);

        var cart = new ShoppingCart();
        cart.add(new Item(apple, 1));
        cart.add(new Item(banana, 5));
        cart.add(new Item(cocoNut, 2));
        cart.add(new Item(orange, 3));

        System.out.println(cart);
        System.out.printf("Total Amount: %.2f kr%n", cart.total());
    }
}
