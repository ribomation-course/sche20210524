import java.time.LocalDate;
import java.time.Month;

import static java.time.LocalDate.of;
import static java.time.Month.*;
import static java.time.temporal.ChronoField.DAY_OF_MONTH;
import static java.time.temporal.TemporalAdjusters.lastDayOfMonth;

public class DateApp {
    public static void main(String[] args) {
        System.out.printf("(a) 2020 Feb: %d%n", lastDayOf(of(2020, FEBRUARY, 1)));
        System.out.printf("(b) 2021 Feb: %d%n", lastDayOf(of(2021, FEBRUARY, 1)));
    }

    static int lastDayOf(LocalDate date) {
        return date.with(lastDayOfMonth()).get(DAY_OF_MONTH);
    }
}
