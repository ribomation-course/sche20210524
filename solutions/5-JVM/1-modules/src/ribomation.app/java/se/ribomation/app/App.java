package se.ribomation.app;

import se.ribomation.numbers.FunctionFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.setupLogger();
        app.parseArgs(args);
        app.run(10);
        app.run(15);
        app.run(20);
    }

    void parseArgs(String[] args) {
        if (args.length > 0) {
            var multiplier = Integer.parseInt(args[0]);
            FunctionFactory.instance.setMultiplier(multiplier);
        }
    }

    void run(int arg) {
        var factory = FunctionFactory.instance;
        var results = factory.functionNames().stream()
                .map(factory::get)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(func -> func.eval(arg))
                .collect(Collectors.toList());

        logger.info(String.format("%d @ %s --> %s",
                arg, factory.functionNames(), results));
    }

    void setupLogger() {
        InputStream is = getClass().getResourceAsStream("/logging.properties");
        try (is) {
            LogManager.getLogManager().readConfiguration(is);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    final Logger logger = Logger.getLogger("app");
}

