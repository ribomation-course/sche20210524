package se.ribomation.numbers;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;

public class FunctionFactory {
    public static FunctionFactory instance = new FunctionFactory();
    private FunctionFactory() { }

    private int multiplier = 10;

    public int getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(int multiplier) {
        this.multiplier = multiplier;
    }

    public Collection<String> functionNames() {
        return Arrays.asList("sum", "fac", "fib", "mult");
    }

    public Optional<NumberFunction> get(String name) {
        switch (name) {
            case "sum":
                return Optional.of(n -> n * (n + 1) / 2);
            case "fac":
                return Optional.of(n -> {
                    long result = 1;
                    for (int k = 1; k <= n; ++k) result *= k;
                    return result;
                });
            case "fib":
                return Optional.of(n -> {
                    long f1 = 0;
                    long f2 = 1;
                    for (int k = 1; k < n; ++k) {
                        long f = f1 + f2;
                        f1 = f2;
                        f2 = f;
                    }
                    return f2;
                });
            case "mult":
                return Optional.of(n -> multiplier * n);
        }
        return Optional.empty();
    }
}

