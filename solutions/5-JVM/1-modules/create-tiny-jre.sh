#!/usr/bin/env bash
set -eux

BLD=./bld

rm -rf $BLD/ribomation-app.jre

jlink --module-path "$JAVA_HOME/jmods;$BLD/jars" \
      --add-modules ribomation.app \
      --output $BLD/ribomation-app.jre \
      --launcher run=ribomation.app/se.ribomation.app.App

set +x
echo '--------------------'
tree -L 2 $BLD/ribomation-app.jre

echo '--------------------'
$BLD/ribomation-app.jre/bin/run

