#!/usr/bin/env bash
set -eux

SRC=./src
BLD=./bld

rm -rf $BLD
mkdir -p $BLD/classes/ribomation.{numbers,app} $BLD/jars

javac --class-path $(ls ./libs/*.jar | tr '\n' ':') \
      --module-path ./libs \
      -d $BLD/classes/ribomation.numbers \
      $(find $SRC/ribomation.numbers -name '*.java')

javac --module-path $BLD/classes \
      -d $BLD/classes/ribomation.app \
      $(find $SRC/ribomation.app -name '*.java')

cp $SRC/ribomation.app/resources/logging.properties $BLD/classes/ribomation.app
	  
jar --create --file $BLD/jars/ribomation.numbers.jar \
    -C $BLD/classes/ribomation.numbers .

jar --create --file $BLD/jars/ribomation.app.jar \
    --main-class se.ribomation.app.App \
    -C $BLD/classes/ribomation.app .

(set +x ; echo '---- BUILT ----------------')
tree $BLD
jar --list --file $BLD/jars/ribomation.numbers.jar
jar --list --file $BLD/jars/ribomation.app.jar

(set +x ; echo '---- EXECUTION ----------------')
java --module-path $BLD/jars --module ribomation.app

