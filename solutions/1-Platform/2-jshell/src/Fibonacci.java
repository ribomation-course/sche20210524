public class Fibonacci {
    static long fibonacci(int n) {
        if (n < 0) throw new IllegalArgumentException("negative: " + n);
        if (n == 0) return 0;
        if (n == 1) return 1;
        return fibonacci(n - 2) + fibonacci(n - 1);
    }

    public static void main(String[] args) {
        var n = args.length == 0 ? 10 : Integer.parseInt(args[0]);
        var f = fibonacci(n);
        System.out.printf("fib(%d) = %d%n", n, f);
    }
}
